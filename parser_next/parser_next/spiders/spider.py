# coding: utf8
import scrapy
import re
import json
import csv
import sys
import logging
from parser_next.items import ParserNextItem

class ParserNextSpider(scrapy.Spider):

    name = "next"

    def start_requests(self):
        url = 'http://www.next.ie/en'
        yield scrapy.Request(url=url, callback=self.parse_category)


    def parse_category(self, response):
        urls = response.xpath(".//a[@class='DepartmentLink']/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse_sub_category)

    def parse_sub_category(self, response):
        urls = response.xpath(".//article/div/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse_sub_sub_category)

    def parse_sub_sub_category(self, response):
        urls = response.xpath(".//article/div/a/@href").extract()
        for url in urls:
            list_url = url.split('/')
            list_url.pop(len(list_url)-1)
            prepare_url = "/".join(list_url)
            yield scrapy.Request(url=response.urljoin(prepare_url), callback=self.parse_set_item)

    def parse_set_item(self, response):
        urls = response.xpath(".//div[@class='SpreadGridSection']/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse_item_list)



    def parse_item_list(self, response):
        urls = response.xpath(".//area/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse_item)



    def parse_item(self, response):
        product = ParserNextItem()
        product['url'] = response.url
        id = response.xpath(".//article[contains(@class,'Selected')]/@data-targetitem").extract_first(default='').replace('-','')
        product['id'] = response.xpath(".//article[contains(@class,'Selected')]/@data-targetitem").extract_first(default='')
        product['img'] = "http://cdn.next.co.uk/Common/Items/Default/Default/ItemImages/AltItemShot/315x472/" + id + ".jpg"
        product['produckt_group'] = response.xpath(".//ul[@class='Breadcrumbs']/li[position()=2]/a/text()").extract_first(
            default='')
        product['name'] = response.xpath(".//div[@class='Title']/h1/text()").extract_first(default='').replace('"','&#34;').replace(',','&#44;').strip()
        try:
            price = response.xpath(
                ".//article[contains(@class,'Selected')]//div[@class='Price']//span/text()").extract_first().split("-")[
                0]
            # if len(price)>10:
            #     price = 0
            product['price'] = float(price.replace('"','&#34;').replace(',','&#44;').replace(u'€','').replace(u',',''))
            if product['id'] != '':
                yield product
        except:
           logging.error("Price error in url: " + product['url'])



















