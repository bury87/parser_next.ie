# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem

class ParserNextPipeline(object):

    item_ids = set()

    def process_item(self, item, spider):
        if item['id'] not in self.item_ids:
            self.item_ids.add(item['id'])
            return item
        else:
            raise DropItem("Duplicate item found: %s" % item['url'])
