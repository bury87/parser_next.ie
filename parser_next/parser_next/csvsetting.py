from scrapy.conf import settings
from scrapy.contrib.exporter import CsvItemExporter
import csv


class CsvOption(CsvItemExporter):

    def __init__(self, *args, **kwargs):
        quoting = settings.get('CSV_QUOTING', csv.QUOTE_ALL)
        kwargs['quoting'] = quoting
        super(CsvOption, self).__init__(*args, **kwargs)